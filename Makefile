F90=gfortran
F90FLAGS=-g -O0
INCLUDE = 
LIBS = 

# Local directory structure
OBJDIR = obj
SRCDIR = src
LIBDIR = lib
INCDIR = include 
EXEDIR = exe
INCLUDE += -I$(INCDIR) 

# HDF5 libraries
HDF5INCS=-I/usr/include
HDF5LIBS=-L/usr/lib/x86_64-linux-gnu/ -lhdf5 -lhdf5_fortran -lhdf5 -lz -lhdf5_hl -lhdf5hl_fortran
INCLUDE += $(HDF5INCS)  
LIBS += $(HDF5LIBS)

# Source files
NRSRCS = nrtype.f90 nrutil.f90 nr_interface.f90 nr.f90 fdjac.f90 fmin.f90 lnsrch.f90 ludcmp.f90 lubksb.f90 newt.f90
UTILS  = h5_output.F90 bisect.f90 funct_fermi1.f90 
EOSSRCS= eos_com.F90 Skyrme_EOS.F90 LS_construction.F90 Tabular_EOS.F90 gibbs_region.F90

SRCS_ := $(NRSRCS) $(UTILS) $(EOSSRCS) 
OBJS_ := ${SRCS_:.F90=.o}
OBJS_ := ${OBJS_:.f90=.o}
OBJS = $(patsubst %,$(OBJDIR)/%,$(OBJS_)) 
SRCS = $(patsubst %,$(SRCDIR)/%,$(SRCS_)) 
 
all: mytov TOV_tabular.a bulk LS gibbs_bound

tabular_test: $(OBJS) $(OBJDIR)/tabular_test.o 
	$(F90) $(F90FLAGS) $(INCLUDE) -o $(EXEDIR)/$@ $^ $(LIBS)
	 
nse_test: $(OBJS) $(OBJDIR)/test_NSE.o 
	$(F90) $(F90FLAGS) $(INCLUDE) -o $(EXEDIR)/$@ $^ $(LIBS)

bulk: $(OBJS) $(OBJDIR)/get_bulk_properties.o
	$(F90) $(F90FLAGS) $(INCLUDE) -o $(EXEDIR)/$@ $^ $(LIBS)

gibbs_bound: $(OBJS) $(OBJDIR)/get_gibbs_boundary.o
	$(F90) $(F90FLAGS) $(INCLUDE) -o $(EXEDIR)/$@ $^ $(LIBS)

LS: $(OBJS) $(OBJDIR)/LSexe.o
	$(F90) $(F90FLAGS) $(INCLUDE) -o $(EXEDIR)/$@ $^ $(LIBS)

skyrme: $(OBJS) $(OBJDIR)/Skyrme_EOS.o $(OBJDIR)/skyrme_exe.o
	$(F90) $(F90FLAGS) $(INCLUDE) -o $(EXEDIR)/$@ $^ $(LIBS)

mytov: $(LIBDIR)/TOV_tabular.a $(OBJDIR)/mytov.o
	$(F90) $(F90FLAGS) $(INCLUDE) -o $(EXEDIR)/$@ $^ $(LIBDIR)/TOV_tabular.a $(LIBS) 

TOV_tabular.a: $(OBJDIR)/TOV_tabular.o 
	ar r $(LIBDIR)/TOV_tabular.a $^ 

clean:
	rm -f $(OBJDIR)/*.o *.mod *.a

$(OBJDIR)/%.o: $(SRCDIR)/%.f90 
	$(F90) -J$(INCDIR) -c $(F90FLAGS) $(INCLUDE) -o $@ $< $(LIBS)

$(OBJDIR)/%.o: $(SRCDIR)/%.F90 
	$(F90) -J$(INCDIR) -c $(F90FLAGS) $(INCLUDE) -o $@ $< $(LIBS)
