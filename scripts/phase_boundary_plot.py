#from scipy.interpolate import interp1d 
import numpy as np 
import glob 
import matplotlib.pyplot as plt 
import pylab
  
files = glob.glob('PhaseBoundary.T*') 

indices = [] 
name = []
fig, ax1 = plt.subplots()
lns = [] 
plt.rc('text',usetex=True) 
#plt.rc('font',family='serif') 
print files 

for idx, fin in enumerate(files):
	indices.append(idx+1) 
	data = np.loadtxt(fin)
	name.append(fin.split("_")[0])
	npl = data[:,1] 
	nnl = data[:,2] 
	nph = data[:,3] 
	nnh = data[:,4] 
        
 	lin = ax1.loglog(npl,nnl,'k') 	
 	lin = ax1.loglog(nnl,npl,'k') 	
 	lin = ax1.loglog(nnh,nph,'k') 	
 	lin = ax1.loglog(nph,nnh,'k') 
 	
	#ntl = npl + nnl 	
	#nth = nph + nnh 
	#yel = npl/ntl 	
	#yeh = nph/ntl 	
	#lin = ax1.semilogx(ntl,yel,'k') 	
 	#lin = ax1.semilogx(ntl,1.0-yel,'k') 	
 	#lin = ax1.semilogx(nth,yeh,'k') 	
 	#lin = ax1.semilogx(nth,1.0-yeh,'k') 
        
	if (idx==0):
		ii = len(data)-50
		np_eq=[]
		nn_eq=[]
		for i in range(1000):
			f = 10**(i/999*13.0 - 13.0)
			np_eq.append(f*npl[ii] + (1-f)*nph[ii]) 
			nn_eq.append(f*nnl[ii] + (1-f)*nnh[ii]) 
		ax1.loglog(np_eq,nn_eq,'ks') 		

		ii = len(data)-80
		np_eq=[]
		nn_eq=[]
		for i in range(1000):
			f = 10**(i/999*11.0 - 11.0)
			np_eq.append(f*npl[ii] + (1-f)*nph[ii]) 
			nn_eq.append(f*nnl[ii] + (1-f)*nnh[ii]) 
		ax1.loglog(nn_eq,np_eq) 		

		
		#ii = len(data)-50
		#ax1.semilogx([ntl[ii],nth[ii]],[yel[ii],yeh[ii]]) 		
		#ii = len(data)-80
		#ax1.semilogx([ntl[ii],nth[ii]],[yel[ii],yeh[ii]]) 		
	

 
ax1.axis([1.e-20, 2.e-1, 1.e-20, 2.e-1])
#ax1.axis([1.e-8, 2.e-1, 0.0, 1.0])
ax1.tick_params(axis='x',labelsize=16) 
ax1.tick_params(axis='y',labelsize=16)
 
ax1.set_xlabel(r'$n_\textrm{p}\,\, (\textrm{fm}^{-3})$',fontsize=16)
ax1.set_ylabel(r'$n_\textrm{n}\,\, (\textrm{fm}^{-3})$',fontsize=16)

#ax1.set_xlabel(r'$n\,\, (\textrm{fm}^{-3})$',fontsize=16)
#ax1.set_ylabel(r'$Y_e$',fontsize=16)

#ax2.set_ylabel('Radius (km)')
plt.show()

#plt.savefig('PhaseBoundaryYe.pdf')
 



